# uses python3

def max_pairwise_product_fast(n, a):
    index1 = 0
    for i in range(1, n):
        if a[i] > a[index1]:
            index1 = i
    
    a[index1], a[n-1] = a[n-1], a[index1]
    
    index2 = 0
    for i in range(1, n-1):
        if a[i] > a[index2]:
            index2 = i
    a[index2], a[n-2] = a[n-2], a[index2]
    
    return a[n-1]*a[n-2]
    
if __name__ == '__main__':
    n = int(input())
    a = [int(x) for x in input().split()]
    assert(len(a) == n)
    print(max_pairwise_product_fast(n, a))
    