# -*- coding: utf-8 -*-
import random
import max_pairwise_product_fast
import max_pairwise_product_naive

random.seed(10)

def stress_test():
    while True:
        n = random.randint(2, 10)
        a = []
        for i in range(n):
            a.append(random.randint(0, 50))
        
        print(a)
        
        result1 = max_pairwise_product_fast.max_pairwise_product_fast(n, a)
        result2 = max_pairwise_product_naive.max_pairwise_product_naive(n, a)
        
        if result1 == result2:
            print('OK')
        else:
            print('Wrong answer: ', result1, result2)
            print()
       

if __name__ == '__main__':
    stress_test()