# Uses python3
def calc_fib(n):
    fib_table = [0, 1]
    for i in range(2, n+1):
        fib_table.append(fib_table[i-1] + fib_table[i-2])
    return fib_table[n]

n = int(input())
print(calc_fib(n))
