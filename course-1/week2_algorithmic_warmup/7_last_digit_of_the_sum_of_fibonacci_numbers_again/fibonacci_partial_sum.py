# Uses python3
import sys

def fibonacci_partial_sum_naive(from_, to):
    sum = 0

    current = 0
    next  = 1

    for i in range(to + 1):
        if i >= from_:
            sum += current

        current, next = next, current + next

    return sum % 10

def get_fibonacci_huge_fast(n, m):
    # store the period string
    period_list = [0, 1]
    # generate the fib seuqunces
    previous = 0
    current = 1
    iters = n
    while iters > 0:
        iters -= 1
        previous, current = current, (previous + current) % m
        
        if current == 0 and ((previous + current) % m) == 1:
            # Maybe period repeats
            period_length = len(period_list)
#            print(period_length)
            
            # Check to see if the period repeats for the existing period length
            temp_list = [current,]

            for _ in range(period_length - 1): 
                previous, current = current, (previous + current) % m
                temp_list.append(current)


            assert(period_length == len(temp_list))
            if period_list == temp_list:
                # found peroid and empty temp_str
                temp_list = []
                return period_list[n % period_length]
            else:
                #seq starts with 01 but is not repaetof period
                # so add to eisting period and empty temp_str
                period_list.extend(temp_list)
                temp_list = []   
        else:
            # add current to perioid
            period_list.append(current)
    # no period repetiotion till now        
    return period_list[n % len(period_list)]
            
def fibonacci_sum_fast(n):
    '''
    S(n) = F(n+2) - 1
    so, S(n) mod 10 = (F(n+2) mod 10 - 1) mod 10
    '''
    val = get_fibonacci_huge_fast(n+2, 10)
    return (val - 1) % 10 

def fibonacci_partial_sum_fast(from_, to):
    '''
    F(m) + ... F(n) = S(n) - S(m-1)    
    '''
    return (get_fibonacci_huge_fast(to + 2, 10) - get_fibonacci_huge_fast(from_+1, 10)) % 10


if __name__ == '__main__':
    input = sys.stdin.read();
    from_, to = map(int, input.split())
    print(fibonacci_partial_sum_fast(from_, to))