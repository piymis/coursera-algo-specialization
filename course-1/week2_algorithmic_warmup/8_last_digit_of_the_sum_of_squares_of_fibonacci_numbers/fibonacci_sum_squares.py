# Uses python3
from sys import stdin

def get_fibonacci_huge_fast(n, m):
    # store the period string
    period_list = [0, 1]
    # generate the fib seuqunces
    previous = 0
    current = 1
    iters = n
    while iters > 0:
        iters -= 1
        previous, current = current, (previous + current) % m
        
        if current == 0 and ((previous + current) % m) == 1:
            # Maybe period repeats
            period_length = len(period_list)
#            print(period_length)
            
            # Check to see if the period repeats for the existing period length
            temp_list = [current,]

            for _ in range(period_length - 1): 
                previous, current = current, (previous + current) % m
                temp_list.append(current)


            assert(period_length == len(temp_list))
            if period_list == temp_list:
                # found peroid and empty temp_str
                temp_list = []
                return period_list[n % period_length]
            else:
                #seq starts with 01 but is not repaetof period
                # so add to eisting period and empty temp_str
                period_list.extend(temp_list)
                temp_list = []   
        else:
            # add current to perioid
            period_list.append(current)
    # no period repetiotion till now        
    return period_list[n % len(period_list)]

def fibonacci_sum_squares_naive(n):
    if n <= 1:
        return n

    previous = 0
    current  = 1
    sum      = 1

    for _ in range(n - 1):
        previous, current = current, previous + current
        sum += current * current

    return sum % 10

def fibonacci_sum_squares_fast(n):
    fib_n = get_fibonacci_huge_fast(n, 10)
    fib_n_1 = get_fibonacci_huge_fast(n+1, 10)
    
    return (fib_n * fib_n_1) % 10
if __name__ == '__main__':
    n = int(stdin.read())
    print(fibonacci_sum_squares_fast(n))
