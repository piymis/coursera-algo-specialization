# Uses python3
import sys

def get_change(m):
    #write your code here
    
    # Algo
    # Safe move: pick the largest value coin
    # find the denominations of the picked coin
    # subtract the calculated value above from the initial
    # Reapeat the same for left value with smaller vaue coins
    
    # sorted values decreasing
    coin_values = [10, 5, 1]
    
    # No denminations (has to change)
    coins = 0
    
    for value in coin_values:
        coins += (m // value)
        m %= value
    
    return coins

if __name__ == '__main__':
    m = int(sys.stdin.read())
    print(get_change(m))
