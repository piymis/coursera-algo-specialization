# Uses python3
import sys

def get_optimal_value(capacity, weights, values):
#    value = 0.
    # write your code here
    
    # calculate value per unit for each weights
    # choose the max of val/weight fully/partially
    # subtract the chosen val from capacity
    # remove the above chosen value from array 
    # repeat the above steps on subproblem
    # O(n^2) implementation
    # To do: O(nlogn) using sorting
    
    val_per_unit = [x[1] / x[0]   for x in zip(weights, values)]
    
    max_value = 0.
    
    while capacity > 0 and len(val_per_unit) > 0:
        # Find max val_per_unit index
        max_index = 0
        for i in range(1, len(val_per_unit)):
            if val_per_unit[max_index] < val_per_unit[i]:
                max_index = i
        # min weight that can still fit
        weight_min = min(capacity, weights[max_index])
        
#        print(val_per_unit)
        max_value += val_per_unit[max_index] * weight_min
        capacity -= weight_min
        
#        print(capacity)
        del val_per_unit[max_index]
        # bug: have to delete all the indices in wights and values
        del weights[max_index]
        del values[max_index]
        
    
    return round(max_value, 4)


if __name__ == "__main__":
    data = list(map(int, sys.stdin.read().split()))
    n, capacity = data[0:2]
    values = data[2:(2 * n + 2):2]
    weights = data[3:(2 * n + 2):2]
    opt_value = get_optimal_value(capacity, weights, values)
    print("{:.10f}".format(opt_value))
