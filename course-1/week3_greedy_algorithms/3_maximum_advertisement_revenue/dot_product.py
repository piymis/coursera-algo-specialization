#Uses python3

import sys

def max_dot_product(a, b):
    #write your code here
    # Greedy approach
    # choose the max in a and max in b
    # multiply the above two values so get the max products
    # remove these two values 
    # solve the subproblem by repeating the same
    # O(n^2)
    # To do: Use sorintg to make it O(nlogn)
    
    res = 0
    for i in range(len(a)):
        max_index_a = max(range(len(a)), key=a.__getitem__) # nlogn
        max_index_b = max(range(len(b)), key=b.__getitem__) # nlogn
        res += a[max_index_a] * b[max_index_b]
        
        # delete max indices
        del a[max_index_a]
        del b[max_index_b]
    return res

if __name__ == '__main__':
    input = sys.stdin.read()
    data = list(map(int, input.split()))
    n = data[0]
    a = data[1:(n + 1)]
    b = data[(n + 1):]
    print(max_dot_product(a, b))
    
