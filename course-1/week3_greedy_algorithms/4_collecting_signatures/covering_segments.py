# Uses python3
import sys
from collections import namedtuple

Segment = namedtuple('Segment', 'start end')

def optimal_points(segments):
    # Algo
    # find the leftmost endpoint in the segments
    # find the number of segments having their left endpoint or or before
    #   the right end point of chosen val above
    # All these segments can be represented by the right endpoint of the chosen
    #   segment
    # Skip through the indices and repeatagain with the rest segments
    
    
    points = []
    #write your code here
    
    # sorted segments so decreses complexity t nlogn
    sorted_indices_start = [x[0] for x in sorted(enumerate(segments), 
                                         key=lambda x: x[1].start)]
    sorted_indices_end = [x[0] for x in sorted(enumerate(segments), 
                                         key=lambda x: x[1].end)]
    print(sorted_indices_start)
    
    # to iterate all the values
    current_index = 0
    for i in range(len(segments)):
        
        # if already reched the end
        if current_index == len(segments):
            break
        
        for j in range(current_index + 1, len(segments)):
            # if next segment is cntained in current then keep moving
            if segments[sorted_indices_start[current_index]].end >= segments[j].start:
                continue
            
            # otherwise increse points and change current index
            
    

if __name__ == '__main__':
    input = sys.stdin.read()
    n, *data = map(int, input.split())
    segments = list(map(lambda x: Segment(x[0], x[1]), zip(data[::2], data[1::2])))
    points = optimal_points(segments)
    print(len(points))
    for p in points:
        print(p, end=' ')
